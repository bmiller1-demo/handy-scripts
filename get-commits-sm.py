#!/usr/bin/env python3

# Objective: Create a list of all users which have performed a commit since the SINCE_TIME date
# Flow:
# 1. Get a list of all projects in an instance
# 2. Iterate over the list of projects and get commits since some date
# 3. Push the commit username into a list
# 4. Print the list to STDOUT

SINCE_TIME = '2021-10-14-01T00:00:00Z'
PAT = ''
URL = ''
commituser_list = []

import gitlab

gl = gitlab.Gitlab(url=URL, private_token=PAT)
gl.auth()

# For each project, get the commits since our SINCE_TIME
projects = gl.projects.list(all=True)
for project in projects:
    commits = project.commits.list(since=SINCE_TIME)
    for commit in commits:
        if commit.author_email not in commituser_list:
            commituser_list.append(commit.author_email)


for user in commituser_list:
    print(user)
